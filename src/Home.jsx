import React, { Component } from 'react';
import {Fab,  useScrollTrigger } from '@material-ui/core';
import axios from 'axios';
import Autorenew from '@material-ui/icons/Autorenew';
import KeyboardBackspace from '@material-ui/icons/KeyboardBackspace';
import Login from './Login';
import Register from './Register';
import './home.scss';

function getCaret(el) { 
  if (el.selectionStart) { 
      return el.selectionStart; 
  } else if (document.selection) { 
      el.focus();
      var r = document.selection.createRange(); 
      if (r == null) { 
          return 0;
      }
      var re = el.createTextRange(), rc = re.duplicate();
      re.moveToBookmark(r.getBookmark());
      rc.setEndPoint('EndToStart', re);
      return rc.text.length;
  }  
  return 0; 
}
export default class Home extends Component {
  constructor() {
    super();
    this.state = {
      id: 0,
      auth: false,
      status: 'מחובר',
      users: [],
      loading: false,
      convers: [],
      chatBox: '',
    }
  }
  
  componentDidMount() {
    fetch('/checkToken')
    .then(res => {
      if (res.status === 200) {
        this.setState({ auth: true });
      } 
    })

    fetch('/getData')
    .then(res =>res.json())
      .then(res => { 
        if(res) {
          const {users, messages} = res
          this.setState({users: users, convers: messages})
        }
      })
  }


  
  sendAnswer = () => {
    const {chatBox, convers,auth} = this.state
    if( chatBox.length >= 2 && auth) {
    this.scrollToBottom()
    var newConvers = convers
    newConvers.push({answer: chatBox,time: new Date})
    this.setState({convers: newConvers, chatBox: ''})
    this.sendToDataBase()
    }
  }

  sendToDataBase = () => {
    var self = this
    axios.post('/api/sendMessage', {
      email: localStorage.email,
      messeage: this.state.chatBox,
      Date: new Date()
    })
    .then(function (response) {
    })
  }

   scrollToBottom = () => {
     if (this.messagesEndRef) {
    let count = this.messagesEndRef.offsetTop + this.messagesEndRef.scrollTop * 10
    this.messagesEndRef.scrollBy({top: count, left: 0, behavior: 'smooth'})
    }
  }
  
  render() {
    const {chatBox,convers, auth,  user, users} = this.state
    console.log('users',users)


    var messegeOptions = (sender, time) => { return (  
    <div class="message-options" style={{
      textAlign:  sender == 0 ? 'left' : 'right'
    }}>
      {/* <span class="message-date">{time ?  time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds(): null}</span> */}
    </div>
) 
}


    var chat = (
      <div className="chat">
      <div className="chat-header clearfix">
       
      <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap',
      width: '100%', alignItems: 'center', justifyContent: 'flex-start'}}>
       {users.map((user, i) => (
            <div style={{border: '1px solid black',
                         padding: '1vh 3vw',
                         margin: '0 2vw'}}>
              {user.name}
            </div>
        ))}
      
         
       </div>
        
        <div className="chat-about">
          <div className="chat-with"></div>
       <div className="chat-num-messages"></div>
        </div>
        <i className="fa fa-star"></i>
      </div>
      
      <div className="chat-history" ref={ref=>this.messagesEndRef = ref}>
        <ul>

      {convers && convers.map((item,i) => 
        <div key={i}>
        {item && (
          <div>
            <div className={item.email === localStorage.email ?
                          "message my-message":
                          "message other-message float-right" 
                          }>
                 {item.messeage}
          </div>
           {messegeOptions(0, item.date)}
           </div>
           
     
        )}
        

          
          </div> )
         }


        

     

     
          
        </ul>
        
      </div>
      
      <div className="chat-message clearfix">
      <div style={{display: 'flex', alignItems: 'center', justifyContent: 'space-evenly'}}>
        <textarea name="message-to-send" id="message-to-send" placeholder ="כתוב כאן את התשובה שלך" rows="3" 
        value={chatBox}
        onKeyUp={(event)=>{
          if (event.keyCode == 13) {
            var content = chatBox;
            var caret = getCaret(this);          
            if(event.shiftKey){
                this.value = content.substring(0, caret - 1) + "\n" + content.substring(caret, content.length);
                event.stopPropagation();
            } else {
                this.value = content.substring(0, caret - 1) + content.substring(caret, content.length);
                this.sendAnswer()
            }
        }
        }}
        
        onChange={(e)=> this.setState({chatBox: e.target.value})}></textarea>
         <Fab disabled={chatBox.length < 1 || !auth}
           onClick={this.sendAnswer.bind(this)}>
              <KeyboardBackspace style={{fontSize: '2rem'}}/>  
          </Fab>   
        </div> 
        <i className="fa fa-file-o"></i> &nbsp;&nbsp;&nbsp;
        <i className="fa fa-file-image-o"></i>
      </div>   
   
   
    
    </div>
    )


 
    return (
    <div>
        {!auth &&(<div>
          <Login />
          <Register />
        </div>)}
        {chat}
    </div>
    )
    
 
  }
}