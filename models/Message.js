const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const MessageSchema = new mongoose.Schema({
  email: { type: String, required: true },
  message :{ type: String, required: true },
  Date: { type: Date, required: true },
});

MessageSchema.pre('save', function(next) {
  if (this.isNew) {
    const document = this;
    console.log(document)
    next();
   }
   else {
    next();
   }
  });

module.exports = mongoose.model('Message', MessageSchema);